<?php

namespace App\Test\Fixture;

use App\User;

class UserFixture
{
    public static function create($quantity = 1, $override = [])
    {
        return factory(User::class, $quantity)->create($override);
    }

    public static function make($quantity = 1, $override = [])
    {
        return factory(User::class, $quantity)->make($override);
    }
}
