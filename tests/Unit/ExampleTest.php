<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * get all User.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->post('/auth', ['cpf' => '05088193181', 'email' => '91910048'])
            ->seeJson([
                'error' => 'invalid_credentials',
            ]);
    }
}
