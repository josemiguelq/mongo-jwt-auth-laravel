<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/user/register', '\App\Http\Controllers\Auth\RegisterController@create');

Route::prefix('user')
    ->middleware('jwt.auth.refresh')
    ->group(function () {
        Route::get('/', '\App\Http\Controllers\UserController@index');
    });

