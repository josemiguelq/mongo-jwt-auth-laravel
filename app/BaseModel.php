<?php
/**
 * Created by PhpStorm.
 * User: josemiguelq
 * Date: 26/12/17
 * Time: 18:22
 */

namespace App;

use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class BaseModel extends Moloquent
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

}