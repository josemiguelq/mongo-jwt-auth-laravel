<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation;
use Illuminate\Support\Collection;
use App\User;


class UserController extends Controller
{

    public function index()
    {
        return User::all();
    }

}
